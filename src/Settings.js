enyo.kind({
    name: "muBlog.Settings",
    kind: enyo.VFlexBox,
    components: [
        //{kind: "PageHeader", components: [{kind: enyo.VFlexBox, content: "muBlog Client - Settings", flex: 1}]},

        {kind: enyo.HFlexBox, pack: "left", style: "width: 70%", components: [
            {kind: enyo.VFlexBox, pack: "center", components: [
                {kind: "RowGroup", components: [
                    {name: "login", kind: "Input", hint: "Type your login"}
                ]},
                {kind: "RowGroup", components: [
                    {name: "passw", kind: "PasswordInput", hint: "Type your password"}
                ]},
                {kind: "Button", caption: "Save & Exit", onclick: "buttonClick"}
            ]}
        ]},

        {name: "errorDialog", kind: "ModalDialog", caption: "Error!", components: [
          {layoutKind: "VFlexLayout", pack: "center", components: [
            {layoutKind: "HFlexLayout", pack: "center", components: [
                {content: "", name: "errorMessage", style: "margin: 0px 0px 20px 0px;", className: "enyo-text-error warning-icon"}
            ]},
            {kind: "Button", caption: "OK", onclick: "okClick"}
          ]}
        ]}
    ],
    okClick: function() {
        this.$.errorDialog.close()
    },
    buttonClick: function() {
        if (this.$.login.getValue().trim() == "") {
            this.$.errorDialog.openAtCenter()
            this.$.errorMessage.setContent("Login cannot be blank")
            return
        }
        if (this.$.passw.getValue() == "") {
            this.$.errorDialog.openAtCenter()
            this.$.errorMessage.setContent("Password cannot be blank")
            return
        }
        enyo.setCookie('user', this.$.login.getValue())
        enyo.setCookie('hash', SHA256(this.$.passw.getValue()))
        this.parent.selectViewByName("messages")
    }
})