var _DEBUG_ = false
var muServer = "http://192.168.1.2"
var timetick = 45000
var maxtimeout = 5000

enyo.kind({
  name: "muBlog.Messages",
  kind: enyo.VFlexBox,
  components: [
        {name: "sendPost", kind: "WebService",
            url: muServer + "/api/add",
            method: "POST",
            timeout: maxtimeout,
            handleAs: "json",
            onSuccess: "sentPost",
            onFailure: "sentPostFailure"},
        
        {name: "getPosts", kind: "WebService",
            url: muServer + "/api/latest",
            method: "GET",
            timeout: maxtimeout,
            handleAs: "json",
            onSuccess: "gotPosts",
            onFailure: "gotPostsFailure"},

        {name: "incomingSound", kind: "Sound", src: "media/message.wav"},
        {name: "sentSound", kind: "Sound", src: "media/sent.wav"},
        {name: "beginSound", kind: "Sound", src: "media/begin.wav"},
        {name: "errorSound", kind: "Sound", src: "media/error.wav"},
        
        {name: "errorDialog", kind: "ModalDialog", caption: "Error!", components: [
          {layoutKind: "VFlexLayout", pack: "center", components: [
            {layoutKind: "HFlexLayout", pack: "center", components: [
                {content: "", name: "errorMessage", style: "margin: 0px 0px 20px 0px;", className: "enyo-text-error warning-icon"}
            ]},
            {kind: "Button", caption: "OK", onclick: "okClick"}
          ]}
        ]},
        {kind: "Header", components: [
            {name: "errorHeader", content: "", className: "enyo-text-error warning-icon", flex: 1},
            {kind: "Button", caption: "Hide", onclick: "hideHeader"}
        ]},
        {kind: "RowGroup", caption: "Post new message", components: [
            {kind: "InputBox", components: [
                {name: "message", kind: "Input", flex: 1},
                {kind: "Button", caption: "Send", onclick: "btnClick"}
            ]}
        ]},
        {kind: "Scroller", flex: 1, components: [
            {name: "list", kind: "VirtualRepeater", onSetupRow: "getListItem",
                components: [
                    {kind: "Item", layoutKind: "VFlexLayout", components: [
                      {name: "title", kind: "Divider"},
                      {name: "description", allowHtml: true}
                    ]}
                ]}
        ]} 
  ],

  create: function() {
        this.inherited(arguments)
        this.results = []
        this.hideHeader()
        var workaround = this
        workaround.timerJob()
        this.$.beginSound.play()
        if (!_DEBUG_)
          this.timerPosts = setInterval(function() {workaround.timerJob()}, timetick)
  },

  timerJob: function() {
    this.$.getPosts.call()
  },

  okClick: function() {
    this.$.errorDialog.close()
  },

  btnClick: function() {
    if (enyo.getCookie('user') && enyo.getCookie('hash')) {
      this.$.sendPost.call({
        user: enyo.getCookie('user'), 
        hash: enyo.getCookie('hash'), 
        text: this.$.message.getValue()})
    }
    else {
        this.$.errorSound.play()
        this.$.errorDialog.openAtCenter()
        this.$.errorMessage.setContent("Not found Contact data. To setup it open the Settings page")
    }
  },
  
  hideHeader: function() {
    this.$.header.hide()
  },

  sentPost: function(inSender, inResponse, inRequest) {
    if (inRequest.xhr.status) {
        if (inResponse.error) {
            this.$.errorSound.play()
            this.$.errorDialog.openAtCenter()
            this.$.errorMessage.setContent(inResponse.error)
        }
        else {
            this.$.sentSound.play()
            this.$.message.setValue("")
            if (!this.results) this.results = []
            this.results.unshift(inResponse)
            this.$.list.render()
        }
    }
    else {
        this.$.errorSound.play()
        this.$.errorDialog.openAtCenter()
        this.$.errorMessage.setContent("Timeout exceeded")
    }
  },

  sentPostFailure: function(inSender, inResponse, inRequest) {
    this.$.errorSound.play()
    this.$.errorDialog.openAtCenter()
    this.$.errorMessage.setContent(inResponse.error)
  },

  gotPosts: function(inSender, inResponse, inRequest) {
    if (inRequest.xhr.status) {
        this.results = inResponse
        this.$.list.render()
    }
    else {
        this.$.errorSound.play()
        this.$.header.show()
        this.$.errorHeader.setContent("Timeout exceeded")
    }
  },

  gotPostsFailure: function(inSender, inResponse, inRequest) {
    this.$.errorSound.play()
    this.$.errorDialog.openAtCenter()
    this.$.errorMessage.setContent(inResponse.error)  
  },

  getListItem: function(inSender, inIndex) {
    if (!this.results) return
    var r = this.results[inIndex]
    if (r) {
      this.$.title.setCaption(r.user + " : " + r.date)
      //this.$.description.setContent(r.text.replace("<\\/", "</"))
      this.$.description.setContent(r.text)
      return true
    }
  }
});
