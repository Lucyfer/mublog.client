enyo.kind({
  name: "muBlog.Client",
  kind: enyo.VFlexBox,
  components: [
    {kind: "PageHeader", components: [
        {kind: enyo.VFlexBox, content: "muBlog Client", flex: 1},
        {name: "settingsButton", kind: "Button", content: "Settings", onclick: "goSettings"},
        {name: "backButton", kind: "Button", content: "Back", onclick: "goBack"}
    ]},
    {name: "pane", kind: "Pane", flex: 1, onSelectView: "viewSelected",
        components: [
            {name: "messages", className: "enyo-bg", kind: "muBlog.Messages"},
            {name: "settings", className: "enyo-bg", kind: "muBlog.Settings"}
    ]},
    {kind: "ApplicationEvents", onOpenAppMenu: "openAppMenuHandler", onCloseAppMenu: "closeAppMenuHandler"},
    {kind: "AppMenu", components: [
        {kind: "EditMenu"},
        {caption: "Settings", onclick: "goSettings"},
        {caption: "Exit", onclick: "exitApp"},
    ]}
  ],
  create: function() {
      this.inherited(arguments);
      this.$.pane.selectViewByName("messages");
  },
  viewSelected: function(inSender, inView) {
      if (inView == this.$.messages) {
          this.$.settingsButton.show()
          this.$.backButton.hide()
      } else if (inView == this.$.settings) {
          this.$.settingsButton.hide()
          this.$.backButton.show()
      }
  },
  goSettings: function() {
      this.$.pane.selectViewByName("settings");
  },
  goBack: function(inSender, inEvent) {
      this.$.pane.back(inEvent);
  },
  exitApp: function() {
    window.close()
  },
  openAppMenuHandler: function() {
    this.$.appMenu.open();
  },
  closeAppMenuHandler: function() {
    this.$.appMenu.close();
  }
});
